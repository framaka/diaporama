
<img class="r-stretch" src="media/laforgeedu-titre-url.png">

---

# Ressource

<img class="r-stretch" src="media/nuit-du-code-groupe.jpg">

----

## Logiciels Libres

----

<img class="r-stretch" src="media/primtux-v8.png">

[PrimTux](https://primtux.fr/) [[source](https://forge.apps.education.fr/primtux)]

----

<img class="r-stretch" src="media/nuitducode.gif">

[La Nuit du Code](https://www.nuitducode.net/) [[source](https://forge.apps.education.fr/nuit-du-code)]

----

<img class="r-stretch" src="media/ecombox.png">

[e-comBox](https://www.reseaucerta.org/pgi/e-combox) [[source](https://forge.apps.education.fr/e-combox)]

----

<img class="r-stretch" src="media/qcmcam.png">

[QCMCam](https://qcmcam.net/) [[source](https://forge.apps.education.fr/sebastien.cogez/qcmcam2)]

----

<img class="r-stretch" src="media/educajou.png">

[Éducajou](https://educajou.forge.apps.education.fr/) [[source](https://forge.apps.education.fr/educajou)]

----

<img class="r-stretch" src="media/ubisit.png">

[Ubisit](https://ubisit.forge.apps.education.fr/ubisit-plan-de-classe-aleatoire) [[source](https://forge.apps.education.fr/ubisit/ubisit-plan-de-classe-aleatoire)]

----

<img class="r-stretch" src="media/mymarkmap2.gif">

[myMarkmap](https://eyssette.forge.apps.education.fr/myMarkmap/) [[source](https://forge.apps.education.fr/eyssette/myMarkmap)]

----

<img class="r-stretch" src="media/flipbook.gif">

[Flipbook](https://eyssette.forge.apps.education.fr/flipbook/) [[source](https://forge.apps.education.fr/eyssette/flipbook)]

----

<img class="r-stretch" src="media/mon-oral.png">

[mon-oral.net](https://www.mon-oral.net/) [[source](https://forge.apps.education.fr/mon-oral)]

----

<img class="r-stretch" src="media/creacarte.png">

[CréaCarte](https://lmdbt.forge.apps.education.fr/creacarte/) [[source](https://forge.apps.education.fr/lmdbt/creacarte)]

----

<img class="r-stretch" src="media/visite-lycee.png">

[Visite virtuelle de lycée](https://lycee-mounier-grenoble.forge.apps.education.fr/visite-virtuelle/) [[source](https://forge.apps.education.fr/lycee-mounier-grenoble/visite-virtuelle)]

----

<img class="r-stretch" src="media/chatmd.png">

[ChatMD](https://eyssette.forge.apps.education.fr/chatMD/) [[source](https://forge.apps.education.fr/eyssette/chatMD)]

----

## Ressources Éducatives Libres

----

<img class="r-stretch" src="media/labomaths.png">

[Laboratoire de mathématiques Grothendieck](https://college-felicien-joly.forge.apps.education.fr/laboratoire-grothendieck/) [[source](https://forge.apps.education.fr/college-felicien-joly/laboratoire-grothendieck)]

----

<img class="r-stretch" src="media/formation-elea-grenoble.png">

[Journée Académique Inspirante Moodle Éléa](https://drane-grenoble.forge.apps.education.fr/jaime/) [[source](https://forge.apps.education.fr/drane-grenoble/jaime)]

----

<img class="r-stretch" src="media/doctrine-technique.png">

[La Doctrine Technique](https://doctrine-technique-numerique.forge.apps.education.fr/) [[source](https://forge.apps.education.fr/doctrine-technique-numerique)]

---

# Communauté

<img class="r-stretch" src="media/i-love-la-forge.jpg">

----

## « Un pour tous, tous pour un ! »

----

<img class="r-stretch" src="media/forge-explore.png">

[Une instance GitLab](https://forge.apps.education.fr/explore)

----

<img class="r-stretch" src="media/portail-apps-education.png">

[apps.education.fr](https://portail.apps.education.fr/)

----

## C'est en forgeant...

----

<img class="r-stretch" src="media/mathalea-code.gif">

[Langage informatique](https://coopmaths.fr/alea/?uuid=ace0a&id=6C10-4&alea=toUT&z=1.5) [[source](https://forge.apps.education.fr/coopmaths/mathalea/-/blob/main/src/exercices/6e/6C10-4.js)]

----

<img class="r-stretch" src="media/site-philo-source-forge.gif">

[Langage pédagogique](https://eyssette.forge.apps.education.fr/cours/) [[source](https://forge.apps.education.fr/eyssette/cours/-/blob/main/philo23/src/README.md)]

----

<img class="r-stretch" src="media/stats-laforgeedu-12mai24.png">

Statistiques du 12 mai 2024

----

Anatomie d'un projet

----

## Synergies

----

<img class="r-stretch" src="media/mathalea-capytale.png">

[MathALÉA dans Capytale](https://coopmaths.fr/alea/) [[source](https://forge.apps.education.fr/coopmaths/mathalea)]

----

<img class="r-stretch" src="media/capytale-mathalea-codepuzzle.png">

[CodePuzzle dans Capytale](https://www.codepuzzle.io/) [[source](https://forge.apps.education.fr/code-puzzle/www-codepuzzle-io)]

----

<img class="r-stretch" src="media/mathalea-elea.jpg">

[MathALÉA dans Éléa](https://coopmaths.fr/alea/) [[source](https://forge.apps.education.fr/coopmaths/mathalea)]

----

<img class="r-stretch" src="media/primtux-educajou.png">

[Éducajou dans PrimTux](https://educajou.forge.apps.education.fr/autobd) [[source](https://forge.apps.education.fr/educajou/autobd)]

----

<img class="r-stretch" src="media/elea-documentation.png">

[Documentation d'Éléa](https://dne-elearning.gitlab.io/moodle-elea/documentation/) [[source](https://forge.apps.education.fr/documentation-elea/documentation-elea.forge.apps.education.fr)]

----

* Dans les coulisses
* Synergies
* Tchap
* JDLE
* DINUM
* Fabrication
* Acculturation / Animation (Dinum, Tchap, AIC) /
* Documentation / DOCS, Vidéo, Formation
* Vidéo
* Formation

----

## Acculturation

----

<img class="r-stretch" src="media/docs-forge.png">

[Documentation de la forge](https://docs.forge.apps.education.fr/) [[source](https://forge.apps.education.fr/docs/docs.forge.apps.education.fr)]

----

<img class="r-stretch" src="media/tube-forge.png">

[Bien commencer avec la Forge](https://tube-numerique-educatif.apps.education.fr/w/vAMyPdtMNRPe8c4TuqhX1d)

----

## Animation

----

[Journée du Libre Éducatif](https://journee-du-libre-educatif.forge.apps.education.fr/) [[source](https://forge.apps.education.fr/journee-du-libre-educatif/journee-du-libre-educatif.forge.apps.education.fr)]

----

<img class="r-stretch" src="media/tchap-laforgeedu.png">

[Tchap](https://www.tchap.gouv.fr/)

---

# Gouvernance

<img class="r-stretch" src="media/migration-forge-benoit-piedallu.jpg">

----

<img class="r-stretch" src="media/primtux-aic.jpg">

[Accélérateur d'Initiatives Citoyennes (DINUM)](https://communs.beta.gouv.fr/)

----

<img class="r-stretch" src="media/la-suite-numerique.gif">

[Appel à Communs](https://lasuite.numerique.gouv.fr/communs)

----

<img class="r-stretch" src="media/gtnum-forges.gif">

[GTnum Forges](https://lium.univ-lemans.fr/gtnum-forges/)

----

<img class="r-stretch" src="media/mooc-iniriaforge.png">

[MOOC Inria](https://learninglab.gitlabpages.inria.fr/mooc-impacts-num/mooc-impacts-num-ressources/)

----

<img class="r-stretch" src="media/recrutement-forge.png">

[Recrutement](https://choisirleservicepublic.gouv.fr/offre-emploi/cheffe-de-projet-communs-numeriques-et-mixite-dne-tn1-hf-reference-2024-1532199/)

----

- Soutien aux projets
- DINUM
- GTnum
- MOOC Inria 
- recrutement

---

Merci de votre attention ;)

<img class="r-stretch" src="media/laforgeedu-vignettes.png">

[forge-communs-numeriques@ldif.education.gouv.fr](mailto:forge-communs-numeriques@ldif.education.gouv.fr)

_« Que la forge soit avec nous ! »_

**[#LaForgeEdu](https://forge.apps.education.fr/)**

---

## 2. Ressources Éducatives Libres

----

<img class="r-stretch" src="images/f-site-philo.gif">

[Cours de philosophie](https://eyssette.forge.apps.education.fr/) [[source](https://forge.apps.education.fr/eyssette/cours/-/blob/main/philo23/src/README.md)]

----

<img class="r-stretch" src="images/f-site-svt-heredite.png">

[Exercices de génétique humaine](https://bio.forge.apps.education.fr/heredite/)

----

<img class="r-stretch" src="images/f-site-svt-heredite-quiz.gif">

[Exercices de génétique humaine](https://bio.forge.apps.education.fr/heredite/aniridie.html) ([source sur la forge](https://forge.apps.education.fr/bio/heredite))

----

<img class="r-stretch" src="images/f-site-texiotheque.png">

[La TeXiothèque](https://lmdbt.forge.apps.education.fr/latexiotheque/)

----

<img class="r-stretch" src="images/f-site-labomaths-reseau.png">

[Le site des labos de maths](https://labo-maths-reseau.forge.apps.education.fr/home/)

----

<img class="r-stretch" src="images/f-site-snt.png">

[Enseigner la SNT](https://ressources.forge.apps.education.fr/snt/)

----

<img class="r-stretch" src="images/f-site-codex.gif">

[CodEx : le code par les exercices](https://codex.forge.apps.education.fr/) [[source](https://forge.apps.education.fr/codex/codex.forge.apps.education.fr)]

----

<img class="r-stretch" src="images/f-site-ada.gif">

[Lire en ligne Ada & Zangemann](https://ada-lelivre.fr/) [[source](https://forge.apps.education.fr/nicolas-taffin-ext/ada-lelivre)]

----

<img class="r-stretch" src="images/f-markpage.png">

[Markpage](https://eyssette.forge.apps.education.fr/markpage/) [[source](https://forge.apps.education.fr/eyssette/markpage)]

---

## 3. Communs Numériques

----

<img class="r-stretch" src="media/gouvernance-forge.png">

---

<img class="r-stretch" src="images/f-site-svt-heredite.png">

[Exercices de génétique humaine](https://bio.forge.apps.education.fr/heredite/)

--- 

<img class="r-stretch" src="images/f-site-labomaths.png">

[Le site d'un labo de maths](https://college-felicien-joly.forge.apps.education.fr/laboratoire-grothendieck/)

---

Le site de tous les labos de maths

<img class="r-stretch" src="images/f-site-labomaths-reseau.png">

[labo-maths-reseau.forge.apps.education.fr](https://labo-maths-reseau.forge.apps.education.fr/home/)

---

##### La Forge des communs numériques éducatifs

<p><font size="70%"><a href="https://college-felicien-joly.forge.apps.education.fr/laboratoire-grothendieck/">college-felicien-joly.forge.apps.education.fr/</a></font></p>

Ceci est exemple de diaporama. Cliquez sur la flèche de droite (en bas à droite) pour passer à la diapositive suivante. Vous pouvez aussi appuyer sur la touche espace ou sur la flèche droite de votre clavier.

---

## Exemple de diaporama

<video data-autoplay src="https://tube-numerique-educatif.apps.education.fr/download/videos/7dfa7532-74ed-43e4-be81-f6e5a707f3db-480.mp4"></video>

Ceci est exemple de diaporama. Cliquez sur la flèche de droite (en bas à droite) pour passer à la diapositive suivante. Vous pouvez aussi appuyer sur la touche espace ou sur la flèche droite de votre clavier.

---

toto

<img class="r-stretch" src="images/f-site-doctrine-technique.png">

<font size="80%"><a href="https://college-felicien-joly.forge.apps.education.fr/laboratoire-grothendieck/">college-felicien-joly.forge.apps.education.fr/</a></font>

---

Le contenu du diaporama est rédigé en markdown. Un language textuel simple et facile à utiliser pour mettre en forme du texte.

<img class="r-stretch" src="images/f-site-svt-heredite.png">

Il est ainsi possible de mettre du texte en **gras** ou en *italique*. 

---

Chaque diapositive est séparée par trois tirets (`---`).

Il est aussi possible de faire des [liens](https://fr.wikipedia.org/), et d'insérer des images :

![](reveal.js/plugin/chalkboard/img/sponge.png)


---

Ainsi que des listes ...

- Un
- Deux
- Trois

---

Du code ...

```python
def hello_world():
    print("Hello World !")
```

---

Et même des maths !

$$ {F}_{A/B} = G \times \frac{M_A M_B}{d^2} $$